local version = "0.2.0-dev"
local modpath = core.get_modpath("weapons_lib")
local srcpath = modpath .. "/src"

weapons_lib = {}
weapons_lib.mods = {}
weapons_lib.settings = {}

dofile(modpath .. "/SETTINGS.lua")

-- general
dofile(srcpath .. "/api/bullets.lua")
dofile(srcpath .. "/api/mods.lua")
dofile(srcpath .. "/api/weapons.lua")
dofile(srcpath .. "/api/weapons_utils.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/HUD/hud_crosshair.lua")
dofile(srcpath .. "/tests/_load.lua")
dofile(srcpath .. "/tests/test1.lua")
dofile(srcpath .. "/tests/test2.lua")
dofile(srcpath .. "/tests/test3.lua")
dofile(srcpath .. "/tests/test4.lua")

weapons_lib.register_mod("weapons_lib")

core.log("action", "[WEAPONS_LIB] Mod initialised, running version " .. version)