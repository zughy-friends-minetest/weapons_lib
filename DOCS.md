# weapons_lib docs

# Table of Contents
* [0. Preface](#0-preface)
  * [0.1 Nomenclature](#01-nomenclature)
* [1. Weapons](#1-weapons)
  * [1.1 Registering a mod](#11-registering-a-mod)
  * [1.2 Registering a weapons](#12-registering-a-weapon)
  * [1.3 Weapon properties](#13-weapon-properties)
    * [1.3.1 Actions structure](#131-actions-structure)
      * [1.3.1.1 Bullets structure](#1311-bullets-structure)
        * [1.3.1.1.1 Built-in bullet entity parameters](#13111-built-in-bullet-entity-parameters)
    * [1.3.2 Metadata](#132-metadata)
    * [1.3.3 Overriding properties](#133-overriding-properties)
  * [1.4 Sounds](#14-sounds)
* [2. API](#2-api)
  * [2.1 Utils](#21-utils)
  * [2.2 Getters](#22-getters)
  * [2.3 Audio](#23-audio)
* [3. About the author](#3-about-the-author)

# 0. Preface
weapons_lib is a Luanti library to easily create different kind of weapons: melee, ranged, fire, hybrid.  

Mod is pretty WIP, so use it at your own risk: things will definitely break.  

Current known limitations:
* No option for bullets to be physical items (although a workaround can be implemented via callbacks)
* Magazines are refilled when restarting the server, as weapons_lib doesn't currently store the status of magazines, except for an internal lua table (emptied when the server goes off, hence the refill)
* Melee weapons aren't designed like in MTG where you can still hit even if the recovery phase is not over - simply dealing less damage. Here it's either full damage (no recovery going on) or no damage (recovery going on). I'm not sure I'm gonna add an option for this, as it would require *a lot* of work

# 0.1 Nomenclature
In order to avoid any confusion, we must distinguish in this document between `weap_def` and `weapon` when they appear in a function.
* `weap_def` is the node registration, from where every `weapon` is generated
* `weapon` is the single itemstack

Furthermore, `w_name` is the complete name of the weapon (e.g. `mymod:cool_weapon`)

# 1. Weapons
## 1.1 Registering a mod
In order to register any weapon, you need to register your mod first via:
```lua
weapons_lib.register_mod("mod_name", rules)
```

This allows you to define the general rules of your weapons. To do that, it uses a table which can contain the following fields (if you don't want to override anything, just declare an empty table):
 * `RELOAD_KEY`: (string) the key used to reload weapons that support such function. By default `"Q"`
	* **BEWARE**: right now, due to MT limitations (see [here](https://github.com/luanti-org/luanti/issues/13954)), it can't be anything but `"Q"`, which also means that weapons can't be dropped. Changing this value will prevent people from reloading, but it'll also allow them to drop weapons
 * `SHOOT_SPEED_MULTIPLIER`: (float) how much players are slowed down when using a weapon that features slowness. Default is `0.66`

## 1.2 Registering a weapon
Weapons can be registered via:
```lua
weapons_lib.register_weapon("mod_name:weapon_name", properties)
```

`properties` is a table containing all the properties that will define the weapon. See the following section to learn about all the available properties.

## 1.3 Weapon properties
Being weapons nodes with extra parameters, all [MT nodes parameters](https://github.com/luanti-org/luanti/blob/master/doc/lua_api.md#item-definition) are supported by default. Some of them, however, act in a peculiar way and it's not possible to override them:
* `range`: (int) melee weapons have `4` by default and it can be overridden. All the rest have `0` and it can't be changed
* `node_placement_prediction` is always `""`
* callbacks `on_use`, `on_secondary_use`, `on_place` and `on_drop` are defined by weapons_lib

Then there are the built-in weapons_lib parameters:
* `weapon_type`: (string) mandatory. It can be `"ranged"`, `"melee"` or `"support"`. A ranged weapon can have melee functions, but not viceversa. Not sure if I'll maintain "support", which is currently not implemented
* `magazine`: (int, no melee) how many bullets the magazine contains; if `-1`, it's infinite
* `limited_magazine`: (bool) whether the weapon shall keep reloading once the magazine has been emptied. Default is `nil`
* `reload_time`: (int, no melee) how much time it takes to reload the weapon. Default is `1`
  * `-1` makes the weapon impossible to reload (forced on melee weapons)
* `sound_reload`: (string, no melee) the sound the weapon does when reloading
* `crosshair`: (string) mandatory, the image of the crosshair
* `slow_down_user`: (boolean) whether the player should be slowed down when attacking and reloading. Default is `false`.
  * **Beware**: changing player's velocity whilst slowed down will break their speed if a fixed value is used. Weapons_lib *cannot* know if there are any external mods interfering, so either rely on multipliers or be very careful when you perform such changes (meaning don't do it if you think they might have a weapon). Related: `wl_slowed_down` metadata
* `can_use_weapon`: (function(player, \<action>)) additional checks about whether the weapon can be used (reloading included). Return `true` to proceed
  * Not called with `"zoom"` actions. Zoom is always allowed (TODO remove it?)
  * reloading doesn't pass any action
  * actions with `continuous_fire` run the check before every bullet/hit
* `can_alter_speed`: (function(player)) additional checks about whether to prevent slowing down/speeding up again the player
  * Slowing down is only checked for `slow_down_user` weapons. Speeding up, however, is checked for all weapons due to async issues (i.e. [here](https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/12))
  * Return `true` to let wapons_lib alter the speed. Related: `wl_slowed_down` metadata
* `on_after_hit`: (function(hitter, weap_def, action, objects_hit, total\_damage)) additional behaviour after all the damage has been inflicted to all the targets of a hit
  * `objects_hit` is a table containing as many tables as the amount of targets hit. Format:
    * `ref`: the ObjectRef
    * `type`: `"player"` or `"entity"`
    * `damage`: the damage that it should have been inflicted. weapons_lib cannot know if an external mod altered the damage of the weapon (e.g. via `core.register_on_punchplayer(..)`)
  * `total_damage`: the sum of the damage that should have been inflicted to all the targets. weapons_lib cannot know if an external mod altered the damage of the weapon (e.g. via `core.register_on_punchplayer(..)`)
* `on_recovery_end`: (function(player, weap_def, action)) additional behaviour for when the weapon is ready to be used again
* `on_reload`: (function(player, weap_def)) additional behaviour when reloading
* `on_reload_end`: (function(player, weap_def, skip_refill)) additional behaviour when the weapon has finished reloading
  * `skip_refill` is a boolean passed when reloading is interrupted (e.g. when switching to another weapon)
* `action1`: (table) action on left click
* `action2`: (table) action on right click
* `action1_hold`: (table, melee only) action on left click if kept pressed for 0.3s. NOT YET IMPLEMENTED, [waiting for MT](https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/7)
* `action2_hold`: (table, melee only) action on right click if kept pressed for 0.3s. NOT YET IMPLEMENTED, ^
* `action1_air`: (table, melee only) action on left click whilst in the air
* `action2_air`: (table, melee only) action on right click whilst in the air

Custom parameters are supported as well. It's recommended putting a `_` at the beginning of the parameter so to avoid conflicts with upcoming official ones

### 1.3.1 Actions structure
Actions are tables containing info about how they should work. Actions also support custom parameters: it's recommended putting a `_` at the beginning of the parameter so to avoid conflicts with upcoming official ones.

* `name`: (string) automatically set, for cross-referencing (e.g. `action_1`, `action1_hold` etc.)
* `type`: (string) mandatory. It can be `"raycast"` (⚡️), `"bullet"` (🥏), `"zoom"` (🔎), `"punch"` (👊) or `"custom"` (TODO: `"parry"`?)
* `damage` (int) ⚡️🥏👊 how much damage deals
  * If a float is passed, weapons_lib will round it to the closest integer once all the optional modifiers (e.g. `decrease_damage_with_distance`) have been calculated. This because Luanti's HP system [cannot deal with float values](https://github.com/luanti-org/luanti/issues/6773) as for now
* `delay`: (float) ⚡️🥏👊 how much time it should pass between being able to rerun the action
* `range`: (float) ⚡️ range of the action
* `fov`: (int) 🔎 fov when zooming (TODO: convert it into a zoom_data table with overlay HUD etc?)
* `bullet`: (table) 🥏 the properties of the physical bullet shot with this action. See the Bullets structure section down below
* `loading_time`: (float) ⚡️🥏👊 how much time before actually running the action. NOT YET IMPLEMENTED, [waiting for MT](https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/8)
* `attack_on_release`: (bool) ⚡️🥏👊🔎 whether it should attack when the action key is released. NOT YET IMPLEMENTED, [waiting for MT](https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/5)
* `knockback`: (int) ⚡️🥏👊 how much knockback the action should inflict. Default is `0`
* `ammo_per_use`: (int) ⚡️🥏 how much ammo is needed to run the action. Default is `1`, except for `"custom"` where is `nil` unless explicitly declared
* `continuous_fire`: (bool) ⚡️🥏 whether it should keep firing when holding down the action key (waiting `delay` seconds between a shot and another). Default is `false`
* `decrease_damage_with_distance`: (bool) ⚡️👊 whether damage should decrease as the distance from the target increases. Default is `false`
* `pierce`: (bool) ⚡️🥏👊 whether the hit should stop on the first person or continue. Default is `false`
* `physics_override`: (table or string) ⚡️🥏👊🔎 how the player physics should change when running the action. Physics is restored when the recovery phase ends
  * It takes either a Luanti physics table or a special string:
    * `"FREEZE"` will block the player, without applying gravity (-9.8)
    * `"DEADWEIGHT"` will block the player, also applying gravity (-9.8)
    * These are just workarounds waiting for https://github.com/luanti-org/luanti/issues/10037
  * If this parameter is declared, `slow_down_user` won't get called
* `sound`: (string) ⚡️🥏👊 the sound to play when the action is run
* `trail`: (table) ⚡️🥏👊 the trail the action should leave. Fields are:
  * `image`: (string) the particle to spawn -- TODO: rendi obbligatorio
  * `amount`: (int) how many particles to draw along the line. In `bullet` type it's the amount to spawn every second. Defaults to `5` in non bullet types and to `2` in bullet types
  * `size`: (int) the size of each particle. Default is `1`
  * `life`: (float) how's gonna last, in seconds. Default is `0.3`
* `on_use`: (function(player, weap_def, action, \<pointed_thing>)) ⚡️🥏👊 additional behaviour when the action is successfully run
  * `pointed_thing` is only passed with custom and punch action types
  * if `continuous_fire` is on, the callback is run repeatedly
  * a must for `"custom"` type to actually do something
* `on_hit`: (function(hitter, target, weap_def, action, hit_point, damage, knockback, proxy_obj)) ⚡️🥏👊 additional behaviour when hitting a player or an entity
  * It must return the damage that will be inflicted (int) and the knockback (int or `nil`). If damage is a float, it'll be rounded to the closest integer
  * `hit_point` only works with type `"raycast"` at the moment, [waiting for MT](https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/6)
* `on_end`: (function(player, weap_def, action)) ⚡️🥏👊 additional behaviour for when the action reaches its end

TODO: physics_override con zoom al momento prob dà problemi
TODO: `explosion`

#### 1.3.1.1 Bullets structure
Bullets are a built-in entity with some extra parameters, like speed and lifetime. Bullets are deallocated if their shooter disconnects.

* `entity`: (table) the luaentity representing the bullet
  * every field declared in the table will be maintained (i.e. `initial_properties`, custom fields) unless it's a weapons_lib special parameter (see next section below)
  * the value of some `initial_properties` is limited:
    * `static_save` is always set to `false`
    * `automatic_face_movement_max_rotation_per_sec` is always set to `360`
    * `automatic_face_movement_dir` is either `0` or any number specified in the declaration
  * if the builtin functions `on_activate` and `on_step` are present, they'll be run after the default behaviour (e.g. collisions check)
* `speed`: (int) the speed of the bullet. Default is `30`
* `lifetime`: (float) how much the bullet should last in case it doesn't disappear first. Default is `5`
* `bounciness`: (float) whether and how much the bullet should bounce when hitting a surface. It goes from `0` (no bounce at all) to `1` (perfect elastic bounce). Default is `0`
* `has_gravity`: (bool) whether the bullet is subject to gravity. Default is `false`
* `weight`: (int) the weight of the bullet, in case `has_gravity` is `true`. Default is `80`
* `friction`: (float) the amount of friction the bullet is subject to when sliding, from `0` (no friction at all) to `1`. It requires `has_gravity`. Default is `0.05`
* `remove_on_contact`: (boolean) whether the bullet should cause an explosion when hitting something. It needs `explosion` -- TODO: perché deve esplodere per forza? `explode_on_contact`?
* `explosion`: (table) TODO
* `on_impact`: (function(shooter, bullet, coll)) TODO

##### 1.3.1.1.1 Built-in bullet entity parameters
After the registration, weapons_lib will assign some special parameters to the entity (so don't use these names, as they will be overridden). This allows the mod to run all the operations it needs, but also modders to know more about the entity:
* `_p_name`: (string) the name of the shooter
* `_action`: (table) the action the entity belongs to
* `_w_name`: (string) the technical name of the weapon it belongs to
* `_is_bullet`: (boolean) always `true`
* `_is_sliding`: (boolean) used for gravity checks

### 1.3.2 Metadata
Weapons communicate their state through players metadata:
* `wl_weapon_state`: (int) communicates the state of the weapon
  * `0`: free
  * `1`: loading, NOT YET IMPLEMENTED
  * `2`: shooting
  * `3`: recovering
  * `4`: reloading
  * `5`: parrying, NOT YET IMPLEMENTED
* `wl_zooming`: (int) whether the player is in zoom state (`1`) or not (`0`)
* `wl_is_speed_locked`: (int) whether the player's speed is altered by a weapon (`physics_override`)
* `wl_slowed_down`: (int) whether the player is currently slowed down by a weapon (shooting, reloading etc)
  * `reset_state(..)` forces it to 0
  * in case of slowing down weapons impeding the alteration of speed (`can_alter_speed` returning `false`), this metadata remains stuck to its latest value until the alteration is possible again. Beware of any manual speed alterations you could have in the meanwhile (e.g. calling a `set_physics_override` in a mod) and be sure to manually set this value once the conditions stopping `can_alter_speed` from happening aren't satisfied anymore
  
### 1.3.3 Overriding properties
`weapons_lib` allows to override weapons/actions properties by using a metadata system applied on the single itemstack. In fact, parameters cannot be overridden by simply using the definition field (e.g. `action.delay = new_value`), as this would change every instance of the weapon, and not just the single itemstack.  

The structure of the metadata is
```
weapon parameter: @field
action parameter: action_name@field
```

In case `field` is a table and you want to override one of its fields (e.g. `something.foo`), the `@` is used to indicate a table (e.g. `action_name@something@foo`)

Currently supported fields:
 * Weapons
   * `@reload_time`
 * Actions
   * `action_name@delay`
   * `action_name@fov`
   * `action_name@bullet@size`: special float parameter to alter the `visual_size`, `collisionbox` and `selectionbox` of a bullet when spawned (by multiplication)

Remember to override the itemstack once you've changed its meta, as you're probably dealing with a copy.

## 1.4 Sounds
Weapon sounds are played to the player calling an action, but also (`audio_lib` only for now) to every player they have attached onto them. In case you want to add custom sounds to custom actions, check out the [Audio section](#23-audio).

# 2. API 
## 2.1 Utils
* `weapons_lib.is_weapon(i_name)`: whether the item `i_name` is a registered weapon
* `weapons_lib.refill(p_name, weapon)`: instantly refills the magazine of `weapon` for `p_name`. If the weapon was reloading, it instantly completes the process
* `weapons_lib.deactivate_zoom(p_name)`: disables the zoom state of the player, if any
* `weapons_lib.reset_state(player)`: resets the weapon state of `player`, instantly completing any ongoing action and setting `wl_slowed_down` to 0
* `weapons_lib.apply_damage(hitter, targets, weap_def, action, proxy_obj)`: useful for `"custom"` actions to deal damage
  * Keep in mind that Luanti HPs *cannot* be float values, so the result after all the calculations (e.g. decrease with distance) will be rounded to the closest integer
  * `targets` is a table of tables, one per target hit. Format of these single tables is `{object = ObjRef, hit_point = intersection_point}`. `hit_point` is optional
  * `proxy_obj` is an entity used to deal damage, i.e. bullets (`hitter` is always a player; in this case, it's the owner)
* `weapons_lib.HUD_crosshair_hide(p_name)`: stops rendering weapon crosshairs for `p_name` until `HUD_crosshair_show(..)` is called
* `weapons_lib.HUD_crosshair_show(p_name)`: starts to render weapon crosshairs again for `p_name`
* `weapons_lib.play_sound(sound, p_name)`: used by default to play built-in weapon sounds. If `p_name` has players attached, sounds will also be played to them

## 2.2 Getters
* `weapons_lib.get_registered_weapons(<mod>)`: returns a table containing the name of all the registered weapons, format `{"w_name1","w_name2"}`. If `mod` is specified, it only returns weapons registerd by such mod
* `weapons_lib.get_weapon_by_name(w_name)`: returns the table of the weapon `w_name`, if any
* `weapons_lib.get_magazine(p_name, w_name)`: returns the current magazine of `p_name`'s weapon `w_name`, if any
* `weapons_lib.get_action_in_progress_weapon(p_name)`: returns a table containing information about the weapon of the action currently in progress, if any; format `{w_name = .., actn_name = ..}`

## 2.3 Audio
* `weapons_lib.play_sound(sound, p_name)`: play a sound to `p_name` and to everyone attached onto them
* `weapons_lib.play_sound_area(sound, pos, range, p_name)`: play a sound at `pos`. If `p_name` is specified, the sound for them won't be positional (used by default for explosions)
* `weapons_lib.stop_sound(sound, p_name)`: as in `play_sound(..)` but to stop a sound

## 3. About the author
I'm Zughy (Marco), a professional Italian pixel artist who fights for free software and digital ethics. If this library spared you a lot of time and you want to support me somehow, please consider donating on [Liberapay](https://liberapay.com/Zughy/). Also, this project wouldn't have been possible if it hadn't been for some friends who helped me testing through: `Giov4`, `MrFreeman`, `_Zaizen_` and `Xx_Crazyminer_xX`
