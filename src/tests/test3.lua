local dmg = 10
local rocket = {
  initial_properties = {
    name = "weapons_lib:bullet_test",
    visual = "mesh",
    mesh = "weaponslib_test_bullet.obj",
    textures = {"weaponslib_test1_trail2.png"},
    collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},

    physical = true,
    collide_with_objects = true,
  },
}



weapons_lib.register_weapon("weapons_lib:test3", {
  description = "Test weapon #3: entity-type bullets\n\n"
                .. "Action 1: shoot rocket (" .. dmg .. " dmg)\n\n"
                .. "Action 2: zoom",

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "weaponslib_test3_sprite.png",
  inventory_image = "weaponslib_test3_sprite.png",

  weapon_type = "ranged",
  magazine = 1,
  reload_time = 1,
  crosshair = "weaponslib_test1_crosshair.png",

  action1 = {
    type = "bullet",
    damage = dmg,
    delay = 0.7,

    continuous_fire = true,
    -- TODO: per pierce, bisogna disattivare collisioni giocator3 (nodi possono rimanere (TODO: vari tipi di pierce es x armi che attraversano muri?))
    -- e controllare raggio attorno a proiettile, sennò non c'è modo di fargli attraversare l'entità/giocatorə

    sound = "weaponslib_test1_attack",
    -- TODO: trail direttamente nel proiettile o mantengo coerenza con armi generiche?
    -- `damage` x es. ora lo prende da qua, non da proiettile; eviterei forse 1 e 1

    trail = {
      image = "weaponslib_test1_trail2.png",
      life = 1,
      size = 2,
      amount = 5, -- 20/amount
    },

    bullet = {
      entity = rocket,

      speed = 30,
      lifetime = 3,

      explosion = { -- TODO: si può portare fuori, x tutte le armi (es. Cannonade); motivo per cui remove_on_contact rimane fuori, solo x proiettili
        range = 4,
        texture = "weaponslib_test1_trail2.png",
      },

      remove_on_contact = true,
      gravity = false,

      -- TODO particelle per quando svanisce? Tipo effetto fumo
    },
  },

  action2 = {
    type = "zoom",
    fov = 24,
    -- TODO
    --HUD = "",
    --sound_in = "",
    --sound_out = "" (facoltativo)
  }
})
