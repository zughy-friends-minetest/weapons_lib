local dmg1      = 3
local dmg1hold  = 6.8
local dmg1air   = 3.7
local dmg2      = 7



weapons_lib.register_weapon("weapons_lib:test2", {
  description = "Test weapon #2: melee\n\nBig knockback\n\n"
                .. "Action 1: punch - push enemies (" .. dmg1hold ..  " dmg)\n\n"
                .. "Action 2: custom - dash forward (" .. dmg2 ..  " dmg)",

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "weaponslib_test2_sprite.png",
  inventory_image = "weaponslib_test2_sprite.png",
  crosshair = "weaponslib_test2_crosshair.png",

  weapon_type = "melee",
  range = 5,

  --[[action1 = {
    type = "punch",
    description = S("slash, @1♥", "<style color=#f66c77>" .. dmg1),
    damage = dmg1,
    delay = 0.4,
    sound = "bl_sword_hit",
  },]]

  -- TEMP: questa dovrebbe diventare action1_hold una volta che:
  --   1. Sarà possibile personalizzare l'animazione dell'oggetto tenuto in mano.
  --   Vedasi https://github.com/luanti-org/luanti/issues/2811.
  --   2. Sarà possibile ritardare l'azione del clic.
  --   Vedasi https://github.com/luanti-org/luanti/issues/13733
  action1 = {
    type = "punch",
    damage = dmg1hold,
    knockback = 40,
    delay = 1,
    sound = "weaponslib_test2_attack"
  },

  action2 = {
    type = "custom",
    damage = dmg2,
    delay = 2.5,
    physics_override = { speed = 0.5, jump = 0 }, -- TODO: rotto, da implementare. Sarebbe buona cosa usare dei moltiplicatori e divisori
    sound = "weaponslib_test2_attack",

    on_use = function(player, weap_def, action)
      local pointed_objects = weapons_lib.get_pointed_objects(player, 5, true)
      local dir = player:get_look_dir()

      dir.y = 0

      local player_vel = player:get_velocity()
      local sprint = vector.multiply(dir,18)

      player:add_velocity(sprint)
      player_vel = vector.multiply(player_vel, -0.7)
      player:add_velocity(player_vel)

      if not pointed_objects then return end
      weapons_lib.apply_damage(player, pointed_objects, weap_def, action)
    end
  }
})