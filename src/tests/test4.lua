local dmg = 10
local grenade = {
  initial_properties = {
    name = "weapons_lib:bullet_test1",
    visual = "mesh",
    mesh = "weaponslib_test_bullet.obj",
    textures = {"weaponslib_test1_trail2.png^[colorize:white:255"},
    collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},

    physical = true,
    collide_with_objects = true
  },

  _collided = false,

  on_step = function(self, dtime, moveresult)
    if moveresult.collides then
      if not self._collided then
        core.chat_send_player(self._p_name, "My first collision!")
        self._collided = true
      end

      core.chat_send_player(self._p_name, "Boing!")
    end
  end
}



weapons_lib.register_weapon("weapons_lib:test4", {
  description = "Test weapon #4: grenade launcher\n\n"
                .. "Action 1: bouncy bullets with gravity and custom messages when colliding (" .. dmg .. " dmg)\n\n",

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "weaponslib_test4_sprite.png",
  inventory_image = "weaponslib_test4_sprite.png",

  weapon_type = "ranged",
  magazine = 1,
  reload_time = 1,
  crosshair = "weaponslib_test1_crosshair.png",

  action1 = {
    type = "bullet",
    damage = dmg,
    delay = 1,

    sound = "weaponslib_test2_attack",

    trail = {
      image = "weaponslib_test1_trail2.png",
      life = 1,
      size = 2,
      amount = 5,
    },

    bullet = {
      entity = grenade,

      speed = 25,
      bounciness = 0.3,
      weight = 60,

      explosion = {
        range = 4,
        texture = "weaponslib_test1_trail2.png",
      },

      remove_on_contact = false,
      has_gravity = true,
    },
  }
})
