if core.get_modpath("audio_lib") then
    audio_lib.register_sound("sfx", "weaponslib_test1_reload", "Weapon reloads")
    audio_lib.register_sound("sfx", "weaponslib_test1_attack", "Weapon shoots")
    audio_lib.register_sound("sfx", "weaponslib_test2_attack", "Weapon swings")
  end
