local dmg1 = 3
local dmg2 = 5
local ammo2 = 3



weapons_lib.register_weapon("weapons_lib:test1", {
  description = "Test weapon #1: raycast\n\nInfinite magazine\nCannot be used if HP <= 10\n\n"
                .. "Action 1: raycast - decrease damage with distance (" .. dmg1 .. " dmg)\n\n"
                .. "Action 2: raycast - slower steadier shot with less reach (" .. dmg2 .. " dmg); consumes " .. ammo2 .. " bullets each",

  wield_scale = {x=1.34, y=1.34, z=1.34},
  wield_image = "weaponslib_test1_sprite.png",
  inventory_image = "weaponslib_test1_sprite.png",

  weapon_type = "ranged",
  magazine = -1,
  reload_time = 2,
  sound_reload = "weaponslib_test1_reload",
  crosshair = "weaponslib_test1_crosshair.png",

  can_use_weapon = function(player, action)
    return player:get_hp() > 10
  end,

  action1 = {
    type = "raycast",
    damage = dmg1,
    range = 30,
    delay = 0.1,
    --fire_spread = 0.2, TODO

    decrease_damage_with_distance = true,
    continuous_fire = true,

    sound = "weaponslib_test1_attack",
    trail = {
      image = "weaponslib_test1_trail1.png",
      size = 2,
      amount = 5
    },
  },

  action2 = {
    type = "raycast",
    damage = dmg2,
    range = 20,
    delay = 0.5,
    ammo_per_use = ammo2,

    continuous_fire = true,

    sound = "weaponslib_test1_attack",
    trail = {
      image = "weaponslib_test1_trail2.png",
      size = 2,
      amount = 5
    },
  }
})
