local function register_action() end
local function calc_action() end
local function wait_for_held_action() end
local function wait_for_charged_action() end
local function can_use_weapon() end
local function set_attack_stance() end
local function slow_down() end
local function speed_up() end
local function run_action() end
local function attack_loop() end
local function decrease_magazine() end
local function attack_hitscan() end
local function attack_melee() end
local function attack_bullet() end
local function attack_end() end
local function recovery_end() end
local function zoom() end
local function reload() end
local function reload_cancel() end
local function reload_end() end
local function draw_particles() end
local function wl_debug() end
local function get_val() end

local debug_mode = weapons_lib.settings.DEBUG_MODE
local debug_p_list = weapons_lib.settings.DEBUG_PLAYERS_LIST


-- ogni volta che un'arma spara, se il suo ritardo è minore di 0.5s, viene eseguita
-- una funzione dopo 0.5s. Tuttavia, se si spara con un arma con ritardo minore e
-- subito dopo con un'altra (prima dei fatidici 0.5s), quella funzione da 0.5s va
-- annullata. Ne tengo traccia con slow_down_func
local slow_down_func = {}       -- KEY: p_name; VALUE: timer func
local reload_func = {}          -- KEY: p_name; VALUE: reload func
local recovery_func = {}        -- KEY: p_name; VALUE: recover_func
local data = {}                 -- KEY: p_name; VALUE: {(string) current_weapon, (ItemStack) current_stack, (string) current_zooming, action_in_progress_weapon = {(string) w_name, (string) actn_name},
                                                        -- magazine = {w_name1 = amount, w_name2 = amount}, last_physics = {MT physics params, _was_slowed_down},
                                                        -- current_recovering = {start_time, length, w_name, actn_name}}
local registered_weaps = {}     -- KEY: w_name; VALUE: {(int) magazine}



----------------------------------------------
--------------INTERNAL USE ONLY---------------
----------------------------------------------

function weapons_lib.init_player(p_name)
  if not data[p_name] then
    data[p_name] = {magazine = {}}

    local p_mags = data[p_name].magazine

    -- TODO: al momento partono sempre con munizioni al massimo, non tiene traccia
    -- dello stato del caricatore (sempre se mai lo farà)
    for w_name, w_data in pairs(registered_weaps) do
      p_mags[w_name] = w_data.magazine or 0
    end
  end
end

----------------------------------------------

--v---------------- globalstep -------------------v--
-- si attiva ogniqualvolta viene cambiato l'oggetto in mano
core.register_globalstep(function(dtime)
  for _, pl_stats in pairs(core.get_connected_players()) do
    local pl_name = pl_stats:get_player_name()
    local pl_data = data[pl_name]
    local player = core.get_player_by_name(pl_name)
    local pl_meta = player:get_meta()
    local weap_state = pl_meta:get_int("wl_weapon_state")
    local wld_item = player:get_wielded_item()
    local w_name = wld_item:get_name()
    local is_weapon = weapons_lib.is_weapon(w_name)
    local prev_w_name = pl_data and pl_data.current_weapon

    -- se aveva un'arma
    if prev_w_name then
      if w_name ~= prev_w_name then
        -- disattivo zoom
        if pl_meta:get_int("wl_zooming") == 1 then
          weapons_lib.deactivate_zoom(player, core.registered_nodes[prev_w_name])
        end

        -- interrompo eventuale ricarica
        if weap_state == 4 then
          reload_cancel(pl_name, core.registered_nodes[prev_w_name], pl_data.current_stack, true)
        end

        -- se il nuovo oggetto è un'arma e sta a secco, comincio la ricarica
        if is_weapon and registered_weaps[w_name].magazine and data[pl_name].magazine[w_name] == 0 then
          reload(player, core.registered_nodes[w_name], wld_item)
        end

        -- se stava caricando o si stava parando, annulla l'azione
        -- TODO: probabilmente meglio far questi controlli sulle funzioni corrispettive,
        -- una volta che queste funzionalità saranno presenti. Qua attack_end rischia
        -- di far danni come già faceva con weap_state == 2 (con controllo infatti
        -- spostato in attack_loop)
        if weap_state == 1 or weap_state == 5 then
          local prev_weap_def = core.registered_nodes[prev_w_name]
          local actn_name = pl_data.action_in_progress_weapon.actn_name
          attack_end(player, prev_weap_def, pl_data.current_stack, prev_weap_def[actn_name])
        end

        if not is_weapon then
          pl_data.current_weapon = nil
          pl_data.current_stack = nil
        else
          pl_data.current_weapon = w_name
          pl_data.current_stack = wld_item
        end

        weapons_lib.HUD_crosshair_update(pl_name, w_name)
      end

    -- se non la aveva e ora la ha
    else
      if is_weapon then
        pl_data.current_weapon = w_name
        pl_data.current_stack = wld_item

        --[[-- se la nuova arma sta a secco, comincio la ricarica
        if registered_weaps[w_name].magazine and data[pl_name].magazine[w_name] == 0 then
          reload(player, core.registered_nodes[w_name], wld_item)
        end]]

        -- se un'arma stava caricando, il mirino mostralo rosso
        weapons_lib.HUD_crosshair_update(pl_name, w_name)
      end
    end
  end
end)
--^---------------- globalstep -------------------^--



-- prevent weapons from picking up items
core.register_on_item_pickup(function(itemstack, picker, pointed_thing, time_from_last_punch,  ...)
  if picker:is_player() then
    local node_in_hand = core.registered_nodes[picker:get_wielded_item():get_name()]

    if node_in_hand and node_in_hand._is_weapon_wl then
      return itemstack
    end
  end
end)



-- TODO: sarebbe bello usare full_punch_interval per segnalare tempo di ricarica, ma non si possono avere tempi di ricarica differenti su singolo oggetto
function weapons_lib.register_weapon(name, def)
  local w_type = def.weapon_type

  assert(w_type, "[WEAPONS_LIB] Missing mandatory field 'weapon_type' in registration of weapon " .. name)
  assert(def.crosshair, "[WEAPONS_LIB] Missing mandatory field 'crosshair' in registration of weapon " .. name)

  -- usato per avere una dichiarazione pulita E al tempo stesso non dover passare
  -- anche il nome in on_use (che lo richiede)
  def.name = name
  def.mod = def.name:sub(1, string.find(def.name, ":") -1)

  def.drawtype = def.mesh and "mesh" or def.drawtype
  def.range = w_type == "melee" and (def.range or 4) or 0
  def.node_placement_prediction = ""    -- disable node prediction
  def.stack_max = 1

  def.reload_time = w_type == "melee" and -1 or (def.reload_time or 1)
  def.slow_down_user = def.slow_down_user == nil and false or def.slow_down_user
  def._is_weapon_wl = true

  -- TODO? slow_down_user potrebbe essere tabella con {shooting, zooming, reloading} per personalizzare quali azioni dovrebbero rallentare

  def.action1       = register_action(name, def.action1, "LMB", "action1")
  def.action1_air   = register_action(name, def.action1_air, "LMB", "action1_air")
  def.action1_hold  = register_action(name, def.action1_hold, "LMB", "action1_hold")
  def.action2       = register_action(name, def.action2, "RMB", "action2")
  def.action2_air   = register_action(name, def.action2_air, "RMB", "action2_air")
  def.action2_hold  = register_action(name, def.action2_hold, "RMB", "action2_hold")

  def.on_use = function(itemstack, user, pointed_thing)
    calc_action(def, itemstack, 1, user, pointed_thing)
  end

  -- RMB = secondary fire
  def.on_secondary_use = function(itemstack, user, pointed_thing)
    calc_action(def, itemstack, 2, user, pointed_thing)
  end

  def.on_place = function(itemstack, user, pointed_thing)
    calc_action(def, itemstack, 2, user, pointed_thing)
  end

  -- Q = reload
  def.on_drop = function(itemstack, user, pos)
    if weapons_lib.mods[def.mod].RELOAD_KEY == "Q" then
      reload(user, def, itemstack)
    else
      return core.item_drop(itemstack, user, pos)
    end
  end

  core.register_node(name, def)
  registered_weaps[name] = {magazine = def.magazine}
end



-- può avere uno o più obiettivi. Formato target = {object, hit_point}
-- proxy_obj = entità, es. proiettile
-- TODO: in futuro altro parametro custom_data tipo x pos quando `explosion` sarà fuori da `bullet`?
function weapons_lib.apply_damage(hitter, targets, weap_def, action, proxy_obj)
  local knockback = action.knockback
  local objects_hit = {}
  local total_damage = 0      -- in caso di più obiettivi colpiti, sommo tutto il danno per poi fare i calcoli alla fine

  if not targets or type(targets) ~= "table" or not next(targets) then return end

  if targets.object then
    targets = {targets}
  end

  -- per ogni obiettivo colpito
  for _, t_data in pairs(targets) do
    local damage = action.damage
    local target = t_data.object

    -- TEMP, to avoid ghost entities; see https://github.com/luanti-org/luanti/issues/13297
    if target:is_player() or target:get_luaentity() then
      if target:get_hp() > 0 then -- TODO: in futuro c'è chi potrebbe voler colpire anche cadaveri (es. nemici) con comportamenti personalizzati (es. punire chi spara a cadaveri)
        -- eventuale danno decrementato a seconda della distanza
        if action.decrease_damage_with_distance then
          local dist = vector.distance(hitter:get_pos(), target:get_pos())
          damage = damage - (damage * dist / action.range)
        end

        if action.bullet and action.bullet.explosion and action.bullet.explosion.decrease_dmg_with_dist then
          local dist = vector.distance(proxy_obj:get_pos(), target:get_pos())
          local dmg_min = 1 + (action.bullet.explosion.minimum_damage_perc or 0) -- 1 = 0; 2 = full dmg
          damage = damage - (damage * dist / action.bullet.explosion.range / dmg_min)
        end

        -- arrotonda danno, Luanti non ragiona in decimali (https://github.com/luanti-org/luanti/issues/6773)
        damage = math.floor(damage + 0.5)

        if action.on_hit then
          damage, knockback = action.on_hit(hitter, target, weap_def, action, t_data.hit_point, damage, knockback, proxy_obj)
          damage = math.floor(damage + 0.5)
        end

        -- eventuale spinta
        if knockback then
          local knk = vector.multiply(hitter:get_look_dir(), knockback)
          target:add_velocity(knk)
        end

        -- applico il danno
        target:punch(hitter, nil, {damage_groups = {fleshy = damage}})
        total_damage = total_damage + damage

        table.insert(objects_hit, {
          ref = target,
          type = target:is_player() and "player" or "entity",
          damage = damage
        })
      end
    end
  end

  if weap_def.on_after_hit then
    weap_def.on_after_hit(hitter, weap_def, action, objects_hit, total_damage)
  end
end



function weapons_lib.refill(p_name, weapon)
  local weap_def = core.registered_nodes[weapon:get_name()]
  local p_meta = core.get_player_by_name(p_name):get_meta()

  if p_meta:get_int("wl_weapon_state") == 4 then
    reload_cancel(p_name, weap_def, weapon)
  end

  data[p_name].magazine[weap_def.name] = weap_def.magazine
end



function weapons_lib.add_ammo(p_name, weapon)
  -- TODO: per aumentare `magazine` quando sarà metadato
end



function weapons_lib.deactivate_zoom(player, weap_def)
  if not weap_def then
    local w_name = data[player:get_player_name()].current_weapon
    if not w_name then return end
    weap_def = core.registered_nodes[w_name]
  end

  local p_meta = player:get_meta()
  local p_name = player:get_player_name()

  if p_meta:get_int("wl_zooming") == 0 then return end

  wl_debug(p_name, "remove zoom | (" .. weap_def.description .. ")")

  --TODO: rimuovere HUD zoom armi
  player:set_fov(0, nil, 0.1) -- TODO: se c'era fov personalizzato, rimettere quello (salvare prop temp ad applicare zoom)
  p_meta:set_int("wl_zooming", 0)
  data[p_name].current_zooming = nil

  if weap_def.slow_down_user and (not weap_def.can_alter_speed or weap_def.can_alter_speed(player))
     and p_meta:get_int("wl_weapon_state") == 0
     and p_meta:get_int("wl_is_speed_locked") == 0 then
    speed_up(player, weap_def.mod)
  end
end



function weapons_lib.reset_state(player)
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()
  local in_prog_weap_info = data[p_name].action_in_progress_weapon

  --if not in_prog_weap_info then return end -- TODO: quando ci sarà wl_weapon_state == 1 salvare arma in action_in_progress_weapon + casistica qui, o qui esplode

  if in_prog_weap_info then
    local w_state = p_meta:get_int("wl_weapon_state")
    local weap_def = core.registered_nodes[in_prog_weap_info.w_name]

    if w_state == 3 then
      local actn_name = in_prog_weap_info.actn_name

      recovery_func[p_name]:cancel()
      recovery_end(player, weap_def, weap_def[actn_name])

    elseif w_state == 4 then
      reload_func[p_name]:cancel()

      if weap_def.sound_reload then
        weapons_lib.stop_sound(weap_def.sound_reload, p_name)
      end

      reload_end(player, weap_def)
    end
  end

  -- in case of weapons impeding the alteration of speed, this value might not be
  -- reset in the recovery / reloading phase, as these functions will skip the check
  -- for slowness if can_alter_speed returns false. Hence the forced set_int here
  p_meta:set_int("wl_slowed_down", 0)
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function weapons_lib.is_weapon(i_name)
  return registered_weaps[i_name]
end





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function weapons_lib.get_registered_weapons(mod)
  local ret = {}

  if not mod then
    for k, _ in pairs(registered_weaps) do
      ret[#ret+1] = k
    end

  else
    for k, _ in pairs(registered_weaps) do
      if core.registered_nodes[k].mod == mod then
        ret[#ret+1] = k
      end
    end
  end

  return ret
end



function weapons_lib.get_weapon_by_name(w_name)
  if not registered_weaps[w_name] then return end

  return core.registered_nodes[w_name]
end



function weapons_lib.get_magazine(p_name, w_name)
  if not data[p_name] or not data[p_name].magazine[w_name] then return end
  return data[p_name].magazine[w_name]
end



function weapons_lib.get_action_in_progress_weapon(p_name)
  local p_data = data[p_name]

  if p_data.action_in_progress_weapon then
    return table.copy(data[p_name].action_in_progress_weapon)
  end
end




----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

-- uso `actn_name` internamente per tenere traccia delle armi con un'azione in corso
function register_action(w_name, action, key, actn_name)
  if not action then return end

  local a_type = action.type

  assert(a_type, "[WEAPONS_LIB] Missing mandatory field 'type' in registration of one of the actions of weapon " .. w_name)

  action.key = key
  action.name = actn_name

  -- TODO ora "bullet", ma poi da tirare fuori
  if action.bullet and action.bullet.explosion then
    action.bullet.explosion.decrease_dmg_with_dist = action.bullet.explosion.decrease_dmg_with_dist == nil and true

    if action.bullet.explosion.sound then
      action.bullet.explosion.sound_range = action.bullet.explosion.sound_range or 16
    end
  end

  if a_type ~= "punch" and a_type ~= "zoom" then
    if a_type ~= "custom" then
      action.ammo_per_use = action.ammo_per_use or 1
    end

    action.delay = action.delay or 0.5
    action.loading_time = action.loading_time or 0

    if a_type == "raycast" then
      action.fire_spread = action.fire_spread or 0
    elseif a_type == "bullet" then
      assert(action.bullet, "Missing bullet in `\"bullet\"` action type")
      weapons_lib.register_bullet(w_name, action)
    end

  elseif a_type == "punch" then
    assert(action.continuous_fire == nil, "Punch actions can't have continuous fire")
  end

  return action
end


-- TODO: weap_def, act_def, weapon e action come nomenclatura. Passare weapon e action in richiami
function calc_action(weap_def, weapon, action_id, player, pointed_thing)
  local is_holdable = ((action_id == 1 and weap_def.action1_hold) or (action_id == 2 and weap_def.action2_hold)) and true
  local in_the_air = weap_def.weapon_type == "melee" and weapons_lib.is_in_the_air(player)
  local action, held_action

  if not in_the_air and is_holdable then
    action = action_id == 1 and weap_def.action1_hold or weap_def.action2_hold
    held_action = true
  else
    if action_id == 1 then
      action = (in_the_air and weap_def.action1_air) and weap_def.action1_air or weap_def.action1
    else
      action = (in_the_air and weap_def.action2_air) and weap_def.action2_air or weap_def.action2
    end
  end

  if not action or not can_use_weapon(player, weap_def, action) then return end

  set_attack_stance(player, weap_def, action)

  -- TODO: funzione per tempo d'attesa, prob sotto va poi messo in funzione a parte

  if action.attack_on_release then
    player:get_meta():set_int("wl_weapon_state", 2)
    wait_for_charged_action(weap_def, weapon, action, action.key, player, action.load_time, 0)

  elseif held_action then
    player:get_meta():set_int("wl_weapon_state", 2)
    wait_for_held_action(weap_def, weapon, action, action.key, player, 0.3)
  else
    run_action(weap_def, weapon, action, player, pointed_thing)
  end
end



function wait_for_held_action(weap_def, weapon, action, held_key, player, countdown)
  core.after(0.1, function()
    if not can_use_weapon(player, weap_def, action, true) then return end

    if player:get_player_control()[held_key] then
      if countdown <= 0 then
        run_action(weap_def, action, player)
      else
        countdown = countdown - 0.1
        wait_for_held_action(weap_def, weapon, action, held_key, player, countdown)
      end
    else
      action = held_key == "LMB" and weap_def.action1 or weap_def.action2
      run_action(weap_def, weapon, action, player)
    end
  end)
end



function wait_for_charged_action(weap_def, weapon, action, held_key, player, load_time, time)
  core.after(0.1, function()
    if not can_use_weapon(player, weap_def, action, nil, true) then return end

    if player:get_player_control()[held_key] then
      if load_time > time then
        time = time + 0.1
      end

      wait_for_charged_action(weap_def, weapon, action, held_key, player, load_time, time)
    else
      run_action(weap_def, weapon, action, player)
    end
  end)
end



function can_use_weapon(player, weap_def, action, held, charged)
  local p_name = player:get_player_name()
  local w_state = player:get_meta():get_int("wl_weapon_state")

  if action.type == "zoom" then
    return w_state ~= 4
  end

  -- controlli aggiuntivi
  if player:get_hp() <= 0 or (weap_def.can_use_weapon and not weap_def.can_use_weapon(player, action)) then
    return end

  if held then
    if w_state > 2 then return end

  elseif charged then
    if w_state > 2 then return end

  else
    local w_magazine = data[p_name].magazine[weap_def.name]

    wl_debug(p_name, "w_state can_use = " .. w_state)

    if w_state ~= 0 or
     (weap_def.magazine and w_magazine ~= -1 and (w_magazine == 0 or action.ammo_per_use > w_magazine)) then
      return end
  end

  return true
end



function set_attack_stance(player, weap_def, action)
  local p_meta = player:get_meta()
  local p_name = player:get_player_name()

  if slow_down_func[p_name] then
    slow_down_func[p_name]:cancel()
  end

  wl_debug(p_name, "set attack stance | (" .. weap_def.description .. ", " .. action.type .. ")")

  if p_meta:get_int("wl_is_speed_locked") == 0 then
    if action.physics_override then
      if action.physics_override == "FREEZE" or action.physics_override == "DEADWEIGHT" then
        local p_pos = player:get_pos()
        local p_y = player:get_look_horizontal()
        local dummy = core.add_entity(p_pos, "weapons_lib:dummy", action.physics_override)
        player:set_attach(dummy, "", nil, {x=0, y=-math.deg(p_y), z=0})
      else
        data[p_name].last_physics = player:get_physics_override()
        data[p_name].last_physics._was_slowed_down = p_meta:get_int("wl_slowed_down")
        player:set_physics_override(action.physics_override)
      end

      p_meta:set_int("wl_is_speed_locked", 1)
    else
      if not weap_def.slow_down_user
         or (weap_def.can_alter_speed and not weap_def.can_alter_speed(player))
         or p_meta:get_int("wl_slowed_down") == 1 then
        return end

      slow_down(player, weap_def.mod)
    end
  end
end



function slow_down(player, mod)
  local p_name = player:get_player_name()

  wl_debug(p_name, "Slow down | Speed before = " .. player:get_physics_override().speed)
  player:set_physics_override({ speed = player:get_physics_override().speed * weapons_lib.mods[mod].SHOOT_SPEED_MULTIPLIER })
  player:get_meta():set_int("wl_slowed_down", 1)
  wl_debug(p_name, "Slow down | Speed now = " .. player:get_physics_override().speed)
end



function speed_up(player, mod)
  local p_name = player:get_player_name()

  wl_debug(p_name, "Speed up | Speed before = " .. player:get_physics_override().speed)
  player:set_physics_override({ speed = player:get_physics_override().speed / weapons_lib.mods[mod].SHOOT_SPEED_MULTIPLIER })
  player:get_meta():set_int("wl_slowed_down", 0)
  wl_debug(p_name, "Speed up | Speed now = " .. player:get_physics_override().speed)
end



function run_action(weap_def, weapon, action, player, pointed_thing)
  local p_meta = player:get_meta()
  local p_name = player:get_player_name()

  wl_debug(p_name, "w_state run action = " .. p_meta:get_int("wl_weapon_state"))

  if action.type == "raycast" or action.type == "bullet" then
    p_meta:set_int("wl_weapon_state", 2)
    data[p_name].action_in_progress_weapon = {w_name = weap_def.name, actn_name = action.name}
    attack_loop(weap_def, weapon, action, player)

  elseif action.type == "punch" or action.type == "custom" then
    p_meta:set_int("wl_weapon_state", 2)
    data[p_name].action_in_progress_weapon = {w_name = weap_def.name, actn_name = action.name}
    attack_loop(weap_def, weapon, action, player, pointed_thing)

  elseif action.type == "zoom" then
    zoom(weap_def, weapon, action, player)

  elseif action.type == "parry" then
    -- player:get_meta():set_int("wl_weapon_state", 5)
    -- TODO
  end
end


function attack_loop(weap_def, weapon, action, player, pointed_thing)
  local p_name = player:get_player_name()

  if action.sound then
    weapons_lib.play_sound(action.sound, p_name)
  end

  if action.type == "punch" then
    attack_melee(player, weap_def, action, pointed_thing)
  else
    if action.ammo_per_use then
      decrease_magazine(player, weap_def, weapon, action.ammo_per_use)
    end

    if action.type == "raycast" then
      attack_hitscan(player, weap_def, action)
    elseif action.type == "bullet" then
      attack_bullet(player, action)
    end
  end

  if action.on_use then
    action.on_use(player, weap_def, action, pointed_thing)
  end

  wl_debug(p_name, "w_state attack loop = " .. player:get_meta():get_int("wl_weapon_state"))

  -- interrompo lo sparo se non è un'arma a fuoco continuo
  if not action.continuous_fire then
    attack_end(player, weap_def, weapon, action)

  else
    local delay = get_val(weapon, action, "delay")
    core.after(delay, function()
      if not core.get_player_by_name(p_name) then return end

      local w_state = player:get_meta():get_int("wl_weapon_state")
      local w_magazine = data[p_name].magazine[weap_def.name]

      if player:get_player_control()[action.key]
         and w_state == 2
         and weap_def.name == data[p_name].current_weapon
         and (weap_def.magazine and (weap_def.magazine == -1 or (w_magazine > 0 and action.ammo_per_use <= w_magazine)))
         and (not weap_def.can_use_weapon or weap_def.can_use_weapon(player, action)) then
        wl_debug(p_name, "w_state launch new attack loop = " .. w_state)
        attack_loop(weap_def, weapon, action, player)
      else
        attack_end(player, weap_def, weapon, action)
      end
    end)
  end
end



function decrease_magazine(player, weap_def, weapon, amount)
  local p_name = player:get_player_name()
  local w_name = weap_def.name
  local mag = data[p_name].magazine

  mag[w_name] = mag[w_name] - amount

  -- automatically reloads if magazine is now empty
  if mag[w_name] == 0 then
    reload(player, weap_def, weapon)
  else
    return true
  end
end



function attack_hitscan(user, weap_def, action)
  local pointed_objects = weapons_lib.get_pointed_objects(user, action.range, action.pierce) -- TODO: considerare raggio illimitato con -1?

  if action.trail then
    local dir = user:get_look_dir()
    local pos_head = vector.add(vector.add(user:get_pos(), vector.new(0,1.475,0)), dir)
    draw_particles(action.trail, dir, pos_head, action.range, action.pierce)
  end

  if pointed_objects then
    weapons_lib.apply_damage(user, pointed_objects, weap_def, action)
  end
end



function attack_melee(user, weap_def, action, pointed_thing)
  if pointed_thing.type ~= "object" then return end

  local object = pointed_thing.ref
  local hit_point = pointed_thing.intersection_point

  weapons_lib.apply_damage(user, {object = object, hit_point = hit_point}, weap_def, action)
end



function attack_bullet(user, action)
  local pos = user:get_pos()
  local pos_head = vector.new(pos.x, pos.y + user:get_properties().eye_height, pos.z)
  local bullet_name = action.bullet.entity.initial_properties.name .. '_entity'
  local bullet = core.add_entity(vector.add(pos_head, user:get_look_dir()), bullet_name, user:get_player_name())

  -- TODO: non si può fare su on_activate quello che segue?
  local bullet_ent = bullet:get_luaentity()

  local speed = bullet_ent._speed
  local dir = user:get_look_dir()

  bullet:set_velocity({
    x=(dir.x * speed),
    y=(dir.y * speed),
    z=(dir.z * speed),
  })

  local yaw = user:get_look_horizontal()
  local pitch = user:get_look_vertical()
  local rotation = ({x = -pitch, y = yaw, z = 0})

  bullet:set_rotation(rotation)
end



function attack_end(player, weap_def, weapon, action)
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  wl_debug(p_name, "w_state attack_end = " .. p_meta:get_int("wl_weapon_state"))

  if p_meta:get_int("wl_weapon_state") == 4 then return end

  p_meta:set_int("wl_weapon_state", 3)

  local w_name = weap_def.name

  -- se sono armi bianche, aggiorno l'HUD qui che segnala che son state usate
  -- TODO: potrebbero essere armi che permettono combo (magari serve un parametro a parte)
  if not weap_def.magazine then
    weapons_lib.HUD_crosshair_update(p_name, w_name, true)
  end

  if action.on_end then
    action.on_end(player, weap_def, action)
  end

  -- finisce attesa e ripristina eventuale fisica personalizzata
  local delay = get_val(weapon, action, "delay")

  data[p_name].current_recovering = {
    start_time = core.get_us_time(),
    length = delay,
    w_name = w_name,
    actn_name = action.name
  }

  recovery_func[p_name] = core.after(delay, function()
    if not core.get_player_by_name(p_name) then return end
    recovery_end(player, weap_def, action)
  end)

  -- ripristina velocità dopo 0.5 secondi
  slow_down_func[p_name] = core.after(0.5, function()
    wl_debug(p_name, "Terminate attack end  | (" .. weap_def.description .. ")")
    if not core.get_player_by_name(p_name)
       -- 1. Non controllo weapon.slow_down_user perché per problemi di asincronia
       -- un attacco può partire prima della fine dello zoom dell'arma che si aveva
       -- prima in mano (vedasi https://gitlab.com/zughy-friends-minetest/weapons_lib/-/issues/12).
       -- Al contrario, controllo wl_slowed_down == 0.
       -- 2. Per casi come quando si smette di sparare con delay < 0.5 e con zoom,
       -- cambiando casella (w_state è 0, w_zooming anche ma già deactivate_zoom ha
       -- velocizzato)
       or p_meta:get_int("wl_slowed_down") == 0
       or (weap_def.can_alter_speed and not weap_def.can_alter_speed(player))
       or p_meta:get_int("wl_weapon_state") ~= 0
       or p_meta:get_int("wl_is_speed_locked") == 1
       or p_meta:get_int("wl_zooming") == 1
      then return end

    speed_up(player, weap_def.mod)
  end)
end



function recovery_end(player, weap_def, action)
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()
  local is_reloading = p_meta:get_int("wl_weapon_state") == 4
  local is_zooming   = p_meta:get_int("wl_zooming") == 1

  wl_debug(p_name, "w_state recovery = " .. p_meta:get_int("wl_weapon_state"))
  wl_debug(p_name, "zooming = " .. p_meta:get_int("wl_zooming"))

  if not is_reloading then
    p_meta:set_int("wl_weapon_state", 0)
  end

  -- non ha senso controllare w_state in generale, vedasi if sopra (le azioni non
  -- si possono sovrapporre)
  local is_w_busy = is_reloading or is_zooming
  local p_data = data[p_name]

  -- se ha la fisica personalizzata, ripristinala
  if p_meta:get_int("wl_is_speed_locked") == 1 then
    p_meta:set_int("wl_is_speed_locked", 0)

    wl_debug(p_name, "Terminate physics override | (" .. weap_def.description .. ")")

    if type(action.physics_override) == "string" then
      local dummy = player:get_attach()
      -- potrei essere morto nel mentre, non avendo più l'entità
      if dummy and dummy:get_luaentity() and dummy:get_luaentity()._wl_dummy then
        dummy:remove()
      end

    else
      local last_physics = p_data.last_physics

      player:set_physics_override(last_physics)

      -- se c'è stato un cambio di velocità tra prima e dopo la sovrascrittura,
      -- applico il cambio a priori
      if last_physics._was_slowed_down ~= p_meta:get_int("wl_slowed_down") then
        wl_debug(p_name, "Physics is different! Slowed down was = " .. last_physics._was_slowed_down  .. " | (" .. weap_def.description .. ")")
        if last_physics._was_slowed_down == 1 then
          speed_up(player, weap_def.mod)
        else
          slow_down(player, weap_def.mod)
        end
      end

      local curr_w_name = p_data.current_zooming or p_data.action_in_progress_weapon.w_name
      local curr_weap_def = core.registered_nodes[curr_w_name]

      -- la velocità non viene alterata mentre la fisica è sovrascritta, ergo
      -- wl_slowed_down rimane 0. Se è partita una ricarica o uno zoom nel mentre
      -- (con l'arma che permette un'alterazione della velocità), rallento ora
      if p_meta:get_int("wl_slowed_down") == 0
         and curr_weap_def.slow_down_user
         and (not curr_weap_def.can_alter_speed or curr_weap_def.can_alter_speed(player))
         and is_w_busy then
        slow_down(player, weap_def.mod)
      end
    end

    -- se prima di quest'azione stavo zoommando e il cambio + attacco è stato
    -- repentino, lo zoom è stato rimosso DOPO l'avvio di quest'azione, non
    -- rivelocizzando lə giocante. Ripristino la velocità (se non sto ricaricando)
    if p_meta:get_int("wl_slowed_down") == 1
       and (not weap_def.can_alter_speed or weap_def.can_alter_speed(player)) -- TODO: messo x torretta BL (no stamina e piazzala -> veloce) ma forse ha poco senso così. Controllo globale?
       and not is_w_busy then
      speed_up(player, weap_def.mod)
    end

  elseif (not weap_def.can_alter_speed or weap_def.can_alter_speed(player))
     and p_meta:get_int("wl_slowed_down") == 1  -- non uso weapon.slow_down_user per lo stesso motivo spiegato in attack_end
     and not is_w_busy then

    if slow_down_func[p_name] then
      wl_debug(p_name, "Cancel 0.5s speed up")
      slow_down_func[p_name]:cancel()
    end

    wl_debug(p_name, "Terminate recovery end | (" .. weap_def.description .. ")")
    speed_up(player, weap_def.mod)
  end

  -- ripristino colore HUD per le armi bianche (faccio qui per non aver un terzo after più in alto)
  if not is_reloading and not weap_def.magazine then
    local curr_weap = p_data.current_weapon
    weapons_lib.HUD_crosshair_update(p_name, curr_weap, false)
  end

  if weap_def.on_recovery_end then
    weap_def.on_recovery_end(player, weap_def, action)
  end

  -- potrebbe essere partita una ricarica nel mentre, salvando nuovi dati
  if not is_reloading then
    p_data.action_in_progress_weapon = nil
  end

  p_data.current_recovering = nil
end



function zoom(weap_def, weapon, action, player)
  local p_meta = player:get_meta()

  if p_meta:get_int("wl_zooming") == 0 then
    local fov = get_val(weapon, action, "fov")

    player:set_fov(fov, nil, 0.1)
    p_meta:set_int("wl_zooming", 1)
    data[player:get_player_name()].current_zooming = weap_def.name
    -- TODO: applica texture, riproduci suono
  else
    weapons_lib.deactivate_zoom(player, weap_def)
  end
end



function reload(player, weap_def, weapon) -- TODO: prob da esporre in futuro
  if weap_def.reload_time == -1 then return end

  local w_name = weap_def.name
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  if player:get_hp() <= 0
     or weap_def.weapon_type == "melee" or not weap_def.magazine
     or weap_def.magazine <= 0 or weap_def.limited_magazine -- TODO: in futuro aggiungere magazine_size, e se magazine è inferiore a size, annullare (AND con limited_magazine)
     or p_meta:get_int("wl_weapon_state") == 4
     or data[p_name].magazine[w_name] == weap_def.magazine
     or (weap_def.can_use_weapon and not weap_def.can_use_weapon(player))
    then return end

  wl_debug(p_name, "Start reloading | (" .. weap_def.description .. ")")

  if weap_def.sound_reload then
    weapons_lib.play_sound(weap_def.sound_reload, p_name)
  end

  local p_data = data[p_name]

  p_data.action_in_progress_weapon = {w_name = weap_def.name, actn_name = nil}
  p_meta:set_int("wl_weapon_state", 4)

  -- rimuovo eventuale zoom
  if p_meta:get_int("wl_zooming", 1) then
    weapons_lib.deactivate_zoom(player, weap_def)
  end

  -- rallenta a prescindere se è bloccato
  local is_locked = player:get_attach() and player:get_attach():get_luaentity()._wl_dummy

  if weap_def.slow_down_user
     and (not weap_def.can_alter_speed or weap_def.can_alter_speed(player))
     and p_meta:get_int("wl_slowed_down") == 0
     and (p_meta:get_int("wl_is_speed_locked") == 0 or is_locked) then
    slow_down(player, weap_def.mod)
  end

  weapons_lib.HUD_crosshair_update(p_name, w_name, true)

  if weap_def.on_reload then
    weap_def.on_reload(player, weap_def)
  end

  local reload_time = get_val(weapon, nil, "reload_time")

  reload_func[p_name] = core.after(reload_time, function()
    if not core.get_player_by_name(p_name) or p_meta:get_int("wl_weapon_state") == 0 then return end -- se è morto nel mentre (meta), annullo

    reload_end(player, weap_def, weapon)
  end)
end



function reload_cancel(p_name, weap_def, weapon, skip_refill)
  wl_debug(p_name, "Cancel reloading | (" .. weap_def.description .. ")")

  reload_func[p_name]:cancel()

  if weap_def.sound_reload then
    weapons_lib.stop_sound(weap_def.sound_reload, p_name)
  end

  reload_end(core.get_player_by_name(p_name), weap_def, weapon, skip_refill)
end



-- TODO: `weapon` mi servirà per personalizzare capienza caricatore
function reload_end(player, weap_def, weapon, skip_refill)
  local p_meta = player:get_meta()
  local p_name = player:get_player_name()

  wl_debug(p_name, "Terminate reloading | (" .. weap_def.description .. ")")

  local p_data = data[p_name]

  -- se è stato annullato ed era ancora in recupero, rifaccio andare il recupero
  -- col tempo rimasto
  if skip_refill and p_data.current_recovering then
    p_meta:set_int("wl_weapon_state", 3)

    local curr_rcv = data[p_name].current_recovering
    local time_passed = (core.get_us_time() - curr_rcv.start_time) / 1000000 -- TEMP: `start_time` si potrà poi sostituire con get_elapsed_time() https://github.com/luanti-org/luanti/pull/15058
    local time_left = curr_rcv.length - time_passed
    local rec_weap_def = core.registered_nodes[curr_rcv.w_name]

    p_data.action_in_progress_weapon = {w_name = curr_rcv.w_name, actn_name = curr_rcv.actn_name}

    if not rec_weap_def.magazine then
      weapons_lib.HUD_crosshair_update(p_name, curr_rcv.w_name, true)
    end

    recovery_func[p_name] = core.after(time_left, function()
      if not core.get_player_by_name(p_name) then return end
      recovery_end(core.get_player_by_name(p_name), rec_weap_def, rec_weap_def[curr_rcv.actn_name])
    end)

  else
    p_meta:set_int("wl_weapon_state", 0)

    if weap_def.slow_down_user
      and (not weap_def.can_alter_speed or weap_def.can_alter_speed(player))
      and p_meta:get_int("wl_is_speed_locked") == 0 then
      speed_up(player, weap_def.mod)
    end
  end

  if not skip_refill then
    p_data.magazine[weap_def.name] = weap_def.magazine
  end

  local curr_weap = p_data.current_weapon

  if not p_data.current_recovering or (core.registered_nodes[p_data.current_recovering.w_name].magazine) then
    weapons_lib.HUD_crosshair_update(p_name, curr_weap, false)
  end

  if weap_def.on_reload_end then
    weap_def.on_reload_end(player, weap_def, skip_refill)
  end

  -- potrebbe esser stato cancellato mentre c'era un recupero in corso
  if not p_data.current_recovering then
    p_data.action_in_progress_weapon = nil
  end
end



function draw_particles(particle, dir, origin, range, pierce)
  local check_coll = not pierce

  core.add_particlespawner({
    amount = particle.amount or 5,
    time = particle.life or 0.3,
    pos = vector.new(origin),
    vel = vector.multiply(dir, range),
    size = particle.size or 1,
    collisiondetection = check_coll,
    collision_removal = check_coll,
    texture = particle.image
  })
end



function wl_debug(p_name, msg)
  if not debug_mode then return end

  if not next(debug_p_list) or debug_p_list[p_name] then
    core.chat_send_player(p_name, msg)
  end
end



function get_val(weapon, action, field)
  if not action then
    return weapon:get_meta():get("@" .. field) or weapon:get_definition()[field]

  else
    return weapon:get_meta():get(action.name .. "@" .. field) or action[field]
  end
end