local function cast_raycast() end
local function get_shooter_left_dir() end
local function get_shooter_up_dir() end
local function debug_particles() end



-- per bloccare fisica
local dummy = {
  initial_properties = {
    physical = true,
    pointable = false,
    collide_with_objects = false,
    visual = "sprite",
    visual_size = {x = 0, y = 0, z = 0},
    collisionbox = {-0.01,0,-0.01,0.01,0,0.01},
    textures = { "blank.png" },
    static_save = false
  },

  _wl_dummy = true,

  -- quando sbuca, se non ha prole, cancellala
  on_activate = function(self, staticdata, dtime_s)
    local obj = self.object

    if staticdata == "DEADWEIGHT" and weapons_lib.is_in_the_air(obj) then
      obj:set_velocity(vector.new(0,-9.8,0))
    end
  end,

  on_step = function(self, dtime, moveresult)
    local obj = self.object

    if obj:get_children() == 0 then
      obj:remove()
    end
  end
}

core.register_entity("weapons_lib:dummy", dummy)



-- I cast a 2x2 grid (assisted aim) to prevent the 0.1 delay. The grid value is
-- actually hardcoded because:
-- 1. it works and I don't need more features from it
-- 2. I haven't got enough knowledge about vectors and rotations anyway
function weapons_lib.get_pointed_objects(shooter, range, has_piercing, entity_table)
  local height, look_dir

  if entity_table then
    height = entity_table.height
    look_dir = entity_table.dir
  else
    height = 1.475
    look_dir = shooter:get_look_dir()
  end

  local hit_pointed_things = {}
  local left_dir = get_shooter_left_dir(shooter)
  local head_up_dir = get_shooter_up_dir(shooter, look_dir)
  local center = shooter:get_pos() + look_dir + {x=0, y=height, z=0}

  local grid_width = 0.46
  local r_amount = 3
  local x_step = (grid_width / r_amount) * (-left_dir)
  local y_step = (grid_width / r_amount) * head_up_dir
  local ray_pos = center - (x_step * (r_amount - 1)) / 2 + (y_step * (r_amount - 1)) / 2

  for row = 1, r_amount do
    for column = 1, r_amount do
      local pthings = cast_raycast(shooter, ray_pos, look_dir, range, has_piercing)
      --debug_particles(look_dir, ray_pos, 30)

      if pthings then
        -- rimuovo giocanti già colpit3 da altro raggio
        for k, possible_target in pairs(pthings) do
          local t_name = possible_target.object:get_player_name() or possible_target.object:get_luaentity().name
          for _, target in pairs(hit_pointed_things) do
            local hit_target_name = target.object:get_player_name() or target.object:get_luaentity().name
            if t_name == hit_target_name then
              pthings[k] = nil
              break
            end
          end
        end

        table.insert_all(hit_pointed_things, pthings)
      end

      ray_pos = ray_pos + x_step
    end

     ray_pos = ray_pos - y_step
     ray_pos = ray_pos - x_step * r_amount
  end

  return hit_pointed_things
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

-- ritorna un array di giocatori con il numero di giocatori trovati a indice 1.
-- Se non trova giocatori diversi da se stesso ritorna nil
function cast_raycast(user, origin, dir, range, has_piercing)
	local a = vector.add(origin, vector.multiply(dir, 0))
	local b = vector.add(origin, vector.multiply(dir, range))
	local ray = core.raycast(a, b)
	local objects = {}

  -- controllo su ogni cosa attraversata dal raycast (da a a b)
	for hit in ray do
    -- se è un oggetto
		if hit.type == "object" then
      -- se è un'entità
      if not hit.ref:is_player() then
        local entity = hit.ref:get_luaentity()
        -- ignora i proiettili -- TODO: in futuro campo su proiettili x dire se colpibili
        if not entity._is_bullet then
          table.insert(objects, {object = hit.ref, hit_point = hit.intersection_point})
        end

      -- se è unə giocante (e non chi spara)
      elseif hit.ref ~= user then
        table.insert(objects, {object = hit.ref, hit_point = hit.intersection_point})
      end

      if not has_piercing then return objects end

    else
      -- se è un nodo mi fermo, e ritorno l'array se > 0 (ovvero ha trovato obiettivi)
      if hit.type == "node" then
        if #objects > 0 then
          return has_piercing and objects or {objects[1]}
        else
          return
        end
      end
		end
	end

  -- se ho sparato a un obiettivo senza incrociare blocchi
  if #objects > 0 then
    return has_piercing and objects or {objects[1]}
  else
    return
  end
end



function get_shooter_left_dir(shooter)
  local yaw = shooter:get_look_horizontal() or shooter:get_yaw()
  local pl_left_dir = vector.new(math.cos(yaw), 0, math.sin(yaw))

  return vector.normalize(pl_left_dir)
end



function get_shooter_up_dir(shooter, dir)
  return vector.rotate_around_axis(dir, get_shooter_left_dir(shooter), math.pi/2)
end



function debug_particles(dir, origin, range)
  core.add_particlespawner({
    amount = 5,
    time = 0.3,
    pos = vector.new(origin),
    vel = vector.multiply(dir, range),
    size = 2,
    texture = "bl_smg_trail.png"
  })
end