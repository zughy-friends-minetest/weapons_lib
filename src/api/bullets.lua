local function generate_entity() end
local function is_colliding() end
local function bullet_explode() end
local function spawn_particles_sphere() end
local function generate_colorized_texture() end



function weapons_lib.register_bullet(w_name, action)
  local bullet = action.bullet
  local bullet_entity = generate_entity(w_name, bullet, action)

  core.register_entity(bullet.entity.initial_properties.name .. "_entity", bullet_entity)

  return bullet_entity
end





----------------------------------------------
---------------ENTITY CREATION----------------
----------------------------------------------

function generate_entity(w_name, def, action)
  local bullet = def.entity

  bullet.initial_properties.static_save = false
  bullet.initial_properties.automatic_face_movement_dir = def.automatic_face_movement_dir or 0 -- offset alla rotazione di base
  bullet.initial_properties.automatic_face_movement_max_rotation_per_sec = 360
  bullet._p_name = "" -- chi spara
  bullet._action = action
  bullet._w_name = w_name
  bullet._damage = action.damage
  bullet._speed = def.speed or 30
  bullet._bounciness = def.bounciness or 0
  bullet._lifetime = def.lifetime or 5
  bullet._curr_lifetime = 0

  bullet._explosion = def.explosion
  bullet._remove_on_contact = def.remove_on_contact

  bullet._bullet_trail = action.trail
  bullet._spawn_particle_time = 0

  bullet._has_gravity = def.has_gravity
  bullet._weight = def.weight or 80
  bullet._friction = def.friction or 0.05

  bullet._on_impact = def.on_impact

  bullet._is_bullet = true
  bullet._is_sliding = false -- per capire se si sta muovendo su superfici piane (granate)

  -- più chiaro di amount, ma al tempo stesso mantengo standard con altre armi nella dichiarazione
  if bullet._bullet_trail then
    bullet._bullet_trail.interval = 20 / (bullet._bullet_trail.amount or 2)
    bullet._bullet_trail.amount = nil
  end

  local custom_on_activate = bullet.on_activate
  local custom_on_step = bullet.on_step



  bullet._destroy = function(self)
    if self._explosion then
      spawn_particles_sphere(self.object:get_pos(), self._explosion.texture)
      bullet_explode(self)
    end

    self.object:remove()
  end



  -- quando si istanzia un'entità
  bullet.on_activate = function(self, staticdata)
    local obj = self.object

    if not staticdata or staticdata == "" then
      obj:remove()
      return
    end

    self._p_name = staticdata -- nome utente come staticdata
    obj:set_armor_groups({immortal = 1}) -- lo imposta come immortale

    local shooter = core.get_player_by_name(self._p_name)
    local weapon = shooter:get_wielded_item()

    if weapon:get_meta():get(self._action.name .. "@bullet@size") then
      local props = obj:get_properties()
      local val = weapon:get_meta():get_float(self._action.name .. "@bullet@size")

      props.visual_size = vector.multiply(props.visual_size, val)
      for _, v in ipairs(props.collisionbox) do
        v = v * val
      end
      for _, v in ipairs(props.selectionbox) do
        v = v * val
      end

      obj:set_properties(props)
    end

    local look_dir = shooter:get_look_dir()
    local yaw = core.dir_to_yaw(look_dir)
    local pitch = math.acos(look_dir.y / vector.length(look_dir)) - math.pi/3

    if not core.is_nan(pitch) then
      obj:set_rotation({x = 0, y = yaw + math.pi/2, z = pitch})
    end

    if custom_on_activate then
      custom_on_activate(self, staticdata)
    end
  end



  bullet.on_step = function(self, dtime, moveresult)
    self._curr_lifetime = self._curr_lifetime + (dtime * 1.6) -- per convertire in secondi

    if self._curr_lifetime >= self._lifetime then
      self:_destroy()
      return
    end

    local owner = core.get_player_by_name(self._p_name)
    local obj = self.object
    local velocity = obj:get_velocity()

    -- dealloca se giocante si disconnette, onde evitare crash su apply_damage per
    -- mancatə `hitter`
    if not owner then
      self.object:remove()
      return
    end

    -- eventuale scia di particelle
    if self._bullet_trail then
      if self._spawn_particle_time >= self._bullet_trail.interval then
        self._spawn_particle_time = 0
        -- Aggiunge le particelle di tracciamento
        core.add_particle({
          pos = obj:get_pos(),
          velocity = vector.divide(velocity, 5),
          acceleration = vector.divide(obj:get_acceleration(), -5),
          expirationtime = self._bullet_trail.life or 0.3,
          size = self._bullet_trail.size,
          texture = self._bullet_trail.image
        })
      else
        self._spawn_particle_time = self._spawn_particle_time + 1
      end
    end

    -- determina se sta toccando terra
    self._is_sliding = moveresult.touching_ground

    -- controllo collisioni
    if moveresult.collides and #moveresult.collisions > 0 then
      local collisions = moveresult.collisions

      if self._remove_on_contact then
        if is_colliding(self, collisions, true) then return end

      elseif self._bounciness > 0 then
        local old_vel = collisions[1].old_velocity

        -- se c'è stata una grande riduzione di velocità, applica l'eventuale rimbalzo
        if not vector.equals(old_vel, velocity) and vector.distance(old_vel, velocity) > 4 then
          if math.abs(old_vel.x - velocity.x) > 5 then
            velocity.x = -old_vel.x * self._bounciness
          end

          if math.abs(old_vel.y - velocity.y) > 5 then
            velocity.y = -old_vel.y * self._bounciness
          end

          if math.abs(old_vel.z - velocity.z) > 5 then
            velocity.z = -old_vel.z * self._bounciness
          end

          obj:set_velocity(velocity)
        end
      end
    end


    -- ferma il proiettile quando colpisce un muro e non è bouncy
    if moveresult.collides and self._bounciness == 0 and not self._is_sliding then
      velocity = velocity / 15
      obj:set_velocity(velocity)
    end

    -- se ha la gravità..
    if self._has_gravity then
      -- ..e sta scivolando, applica frizione
      if self._is_sliding then
        -- il framerate di test è 60, quindi 0.017 è il tempo tra un frame e l'altro
        local test_step = 0.017
        -- indipendente dal framerate
        local friction_factor = math.pow((1 - self._friction), (dtime / test_step))

        velocity.x = velocity.x * friction_factor
        velocity.z = velocity.z * friction_factor

        obj:set_velocity(velocity)
        obj:set_acceleration(vector.multiply(obj:get_acceleration(), self._bounciness))
      -- sennò calcola accelerazione
      else
        local accel_step = vector.new(0, -self._weight * dtime, 0)
        local accel = vector.add(obj:get_acceleration(), accel_step)
        obj:set_acceleration(accel)
      end
    end

    -- ricalcola velocità
    velocity = obj:get_velocity()

    local max_vertical_speed = -self._weight * 3.4
    local max_speed = -max_vertical_speed * 2.4

    -- limita se cade troppo velocemente..
    if velocity.y < max_vertical_speed then
      velocity.y = max_vertical_speed
    end

    local current_speed = vector.length(velocity)

    -- ..o se va troppo veloce
    if current_speed > max_speed then
      local dir = vector.normalize(velocity)
      velocity = vector.multiply(dir, max_speed)
    end

    obj:set_velocity(velocity)

    -- orienta il proiettile a seconda della sua direzione
    local speed = vector.length(velocity)

    if speed > 0.5 then
      local direction = vector.normalize(velocity)
      local yaw = core.dir_to_yaw(direction)
      local pitch = math.acos(velocity.y / speed) - math.pi/3
      local rotation = obj:get_rotation()

      if not core.is_nan(pitch) then
        obj:set_rotation({x = 0, y = yaw + math.pi/2, z = rotation.z})
      end
    end

    if custom_on_step then
      custom_on_step(self, dtime, moveresult)
    end
  end

  return bullet
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function is_colliding(bullet, collisions, remove_on_contact)
  local has_hit = false
  for _, coll in pairs(collisions) do
    if coll.object then
      local obj = coll.object
      if obj:is_player() then
        -- se non colpisco me stessə
        if obj:get_player_name() ~= bullet._p_name then
          weapons_lib.apply_damage(core.get_player_by_name(bullet._p_name), {object = obj}, core.registered_nodes[bullet._w_name], bullet._action)
          has_hit = true

        -- sennò..
        else
          if bullet._lifetime < (15 / bullet._speed) then -- TODO: controlla, mi sa di cagata
            obj:set_velocity({
              x = coll.old_velocity.x,
              y = coll.old_velocity.y,
              z = coll.old_velocity.z,
            })
          end
        end

      else
        local entity = obj:get_luaentity()
        -- se collide con un altro proiettile, esplodono entrambi
        if entity and entity._is_bullet then
          has_hit = true
          entity:_destroy()
        end
      end

    elseif coll.type == "node" then
      has_hit = true

      if bullet._on_impact then
        local shooter = core.get_player_by_name(bullet._p_name)
        bullet._on_impact(shooter, bullet.object, coll)
      end
    end
  end

  if has_hit and remove_on_contact then
    bullet:_destroy()
    return true
  end
end



function bullet_explode(self)
  local explosion = self._explosion
  local range = explosion.range
  local origin = self.object:get_pos()
  local p_name = self._p_name

  if not origin then return end

  local objs = core.get_objects_inside_radius(origin, range)
  local targets = {}
  local entities = {}

  -- Se ho colpito qualcosa
  if objs then
    for _, obj in ipairs(objs) do
      if obj:is_player() then
        table.insert(targets, {object = obj}) -- TODO: come calcolare hit_point?

      elseif obj ~= self.object and obj:get_luaentity() then
        local entity = obj:get_luaentity()
        table.insert(entities, entity)

        if entity.initial_properties and entity.initial_properties.collide_with_objects and not entity._is_bullet then
          table.insert(targets, {object = obj}) -- TODO: come calcolare hit_point?
        end
      end
    end

    if explosion.sound then
      weapons_lib.play_sound_area(explosion.sound, self.object:get_pos(), explosion.sound_range, p_name)
    end

    weapons_lib.apply_damage(core.get_player_by_name(p_name), targets, core.registered_nodes[self._w_name], self._action, self.object)
  end

  if #entities == 0 then return end

  self.object:remove()
  for _,entity in pairs(entities) do -- rimuovi proiettili coinvolti nell'esplosione
    if entity._is_bullet then
      entity:_destroy()
    end
  end
end



function spawn_particles_sphere(pos, texture)
  if not pos then return end

  local s = 7.5 -- speed
  core.add_particlespawner({
    amount = 85,
    time = 0.1,
    pos = pos,
    vel = {min = vector.new(-s, -s, -s), max = vector.new(s, s, s)},
    acc =  {min = vector.new(-s, -s, -s), max = vector.new(s, s, s)},
    exptime = 0.2,
    size = {min = 2, max = 9},
    collisiondetection = false,
    vertical = false,
    glow = 12,
    texture = generate_colorized_texture(texture, {r=0, g=0, b=0, a=0}),
    texpool = {
      generate_colorized_texture(texture, {r=0, g=0, b=0, a=0}),
      generate_colorized_texture(texture, {r=0, g=0, b=0, a=255*0.1}),
      generate_colorized_texture(texture, {r=255, g=255, b=255, a=255*0.1}),
    }
  })
end



function generate_colorized_texture(texture, color)
  return {
    name = texture .. "^[colorize:" .. core.colorspec_to_colorstring(color),
    alpha_tween = {1, 0.6},
    scale_tween = {
      {x = 1, y = 1},
      {x = 0.5, y = 0.5}
    }
  }
end