core.register_on_joinplayer(function(player)
  local p_name = player:get_player_name()

  weapons_lib.init_player(p_name)
  weapons_lib.HUD_crosshair_create(p_name)

  local p_meta = player:get_meta()

  p_meta:set_int("wl_weapon_state", 0)
  p_meta:set_int("wl_zooming", 0)
  p_meta:set_int("wl_is_speed_locked", 0)
  p_meta:set_int("wl_slowed_down", 0)
end)



core.register_on_dieplayer(function(player, reason)
  weapons_lib.deactivate_zoom(player)
  weapons_lib.reset_state(player)

  player:get_meta():set_int("wl_weapon_state", 0) -- in teoria non necessario, in pratica non mi fido ancora dello stato del codice
end)