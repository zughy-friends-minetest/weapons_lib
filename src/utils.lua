function weapons_lib.is_in_the_air(obj_ref)
  local obj_pos = obj_ref:get_pos()
  local node_beneath = vector.new(obj_pos.x, obj_pos.y - 0.4, obj_pos.z)
  local is_in_the_air = core.get_node(node_beneath).name == "air"

  return is_in_the_air
end



local sound_handles = {}
local IS_AUDIO_LIB = core.get_modpath("audio_lib")


function weapons_lib.play_sound(sound, p_name)
  if IS_AUDIO_LIB and audio_lib.is_sound_registered(sound) then
    local targets = {p_name}
    local player = core.get_player_by_name(p_name)

    if not player then return end

    for _, obj in ipairs(player:get_children()) do
      if obj:is_player() then
        targets[#targets+1] = obj:get_player_name()
      end
    end

    --audio_lib.play_sound(sound, {object = core.get_player_by_name(p_name), exclude_players = targets})
    audio_lib.play_sound(sound, {to_players = targets})
  else
    sound_handles[sound .. "@" .. p_name] = core.sound_play(sound, {to_player = p_name})
  end
end



function weapons_lib.play_sound_area(sound, pos, range, p_name)
  if IS_AUDIO_LIB and audio_lib.is_sound_registered(sound) then
    local targets = {p_name}
    local player = core.get_player_by_name(p_name)

    if not player then return end

    for _, obj in pairs(player:get_children()) do
      if obj:is_player() then
        targets[#targets+1] = obj:get_player_name()
      end
    end

    audio_lib.play_sound(sound, {pos = pos, max_hear_distance = range, exclude_players = targets})
    audio_lib.play_sound(sound, {to_players = targets})
  else
    core.sound_play(sound, {pos = pos, max_hear_distance = range, exclude_player = p_name})
    if p_name then
      core.sound_play(sound, {to_player = p_name})
    end
  end
end



function weapons_lib.stop_sound(sound, p_name)
  if IS_AUDIO_LIB and audio_lib.is_sound_registered(sound) then
    local player = core.get_player_by_name(p_name)

    if not player then return end

    for _, obj in ipairs(player:get_children()) do
      if obj:is_player() then
        audio_lib.stop_sound(obj:get_player_name(), sound)
      end
    end

    audio_lib.stop_sound(p_name, sound)
  else
    core.sound_stop(sound_handles[sound .. "@" .. p_name])
  end
end