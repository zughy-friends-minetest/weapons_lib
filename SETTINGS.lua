-- print detailed info in chat about weapons being used. Pretty verbose
weapons_lib.settings.DEBUG_MODE = false

-- who shall see the debug messages. Leave empty for the whole server.
-- Format { p_name = true, pl_name = true}
weapons_lib.settings.DEBUG_PLAYERS_LIST = {}